#Run command script to install lynis from ssm. Then run lynis save results to an EFS mount target

#!/bin/bash
yum install lynis
sudo mkdir /home/ec2-user/home/home-directories/Audit_Reports
sudo lynis audit system >> /home/ec2-user/home/home-directories/Audit_Reports/lynis.log
sudo chown -R ec2-user:ec2-user /home/ec2-user/home/home-directories/Audit_Reports/