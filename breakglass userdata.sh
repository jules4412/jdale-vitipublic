#!/bin/bash

adduser break-glass
sudo usermod -a -G wheel break-glass
sudo mkdir -p /home/break-glass/.ssh
sudo chmod 700 /home/break-glass/.ssh
sudo echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDg4Tss2eHWT2Z/6SYYMNb2hzmcl0hoxckGhTf3KVIVpRFrfRks3Qnh2oGycey2icwYfrun1vWm6rmUug9hZTVTHfsabj++xylEu/8XuQto0HeWp9tk4iwNku6EZ+JBzuR7iHrVZcwqSCQn5ln9SaKALOuCqoDNQq/gWIYPmZVE0WD+66pIfKuUvaADNpVecn4B+AGsfBgAeKVz63zXqxbR8Y3Hjl2rvVfGBSGj9kxykH9klI0ew3falGh3D5JNpxXRyWg2u9LkLXEqqXRJXF9JqRKT0ZqTLaprWqOe6U7DoHV8ktyYyE6F1WqwjU4g0f8+gYQ8DgQK8ijcwnZmnfAo21RniftymQRoGx6rr3o0TshA61WwC/lI90jnLakt89KWlEvMtb4jWkL0a9NuaUQf21c29lYvQfLSRyqqMXanGrkcrhCG0iY39gwAYVPMotulycd4BEOxv9sVDCoeHcDkJZw4XYVpSJvI1+vh41gquJJLEuA3Sq997uxSrOSjCPncOqSeOd63TJYXYk1hR6
rbkipoqwglDrRbWKFZo+aWYpBISDV/ap0a9S4c3ltRrwJN6BgI5ZvHWEw0wUQA/VKnRlFtnvNYylIlMFnDjzDhgRxpunvmPpaz1cl1uiM398mNuOyQikxxqZovdkEYveJWssOM/V5ulY9HYGTaRAEO1w== jdale@MBP-M1.local " >> /home/break-glass/.ssh/authorized_keys
sudo chmod 600 /home/break-glass/.ssh/authorized_keys
sudo echo "PubkeyAuthentication yes" >> /home/ec2-user/.ssh/config
sudo echo "PasswordAuthentication no" >> /home/ec2-user/.ssh/config
sudo systemctrl restart sshd
getent passwd break-glass > /home/ec2-user/home/home-directories/usercreationcheck

#you can execute shell script using AWS Systems Manager > Run Command > Run a command, Select the type of command that you want to run & Search by keyword " AWS-RunShellScript " (Linux platform only).

#In Command Parameters, specify a shell script or a command to run.

# PS : For both Windows & Linux platform, search by keyword " AWS-RunPowerShellScript "