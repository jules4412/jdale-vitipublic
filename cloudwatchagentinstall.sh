#!/bin/bash
#Make sure ec2 instance has access to CW and SSM
sudo yum install amazon-cloudwatch-agent -y
#location
#/opt/aws/amazon-cloudwatch-agent
sudo amazon-linux-extras install collectd -y #awslinux2023
sudo yum install collectd -y  #awslinux2
sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -c ssm:AmazonCloudWatch-linux -s
sudo systemctl start amazon-cloudwatch-agent.service
sudo systemctl enable amazon-cloudwatch-agent.service
sudo systemctl status amazon-cloudwatch-agent.service
