#!/bin/bash

Update the system
yum update -y

Install git and httpd
yum install -y git httpd.x86_64

Start and enable the httpd service
systemctl start httpd.service
systemctl enable httpd.service

Configure git to use the AWS CodeCommit credential helper
cd /home/ec2-user
sudo git config --system credential.helper '!aws codecommit credential-helper $@'
sudo git config --system credential.UseHttpPath true

Clone the code from codecommit repository
sudo git clone https://git-codecommit.us-east-1.amazonaws.com/v1/repos/websiterepo1 /var/www/html
#Install Codedeploy agent
sudo yum install ruby -y
sudo yum install -y aws-cli
cd /home/ec2-user/
aws s3 cp 's3://aws-codedeploy-us-east-1/latest/codedeploy-agent.noarch.rpm' . --region us-east-1
sudo yum -y install codedeploy-agent.noarch.rpm

