#!/bin/bash

yum update -y

#Install git and httpd
yum install -y git httpd
yum install -y aws-cli

Start and enable the httpd service
systemctl start httpd.service
systemctl enable httpd.service

#Configure git to use the AWS CodeCommit credential helper
cd /home/ec2-user
sudo git config --system credential.helper '!aws codecommit credential-helper $@'
sudo git config --system credential.UseHttpPath true

#Clone the code from codecommit repository
sudo git clone https://git-codecommit.us-east-1.amazonaws.com/v1/repos/websiterepo1 /var/www/html
