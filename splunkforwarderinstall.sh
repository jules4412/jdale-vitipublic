wget -O splunkforwarder-9.1.2-b6b9c8185839-Linux-x86_64.tgz "https://download.splunk.com/products/universalforwarder/releases/9.1.2/linux/splunkforwarder-9.1.2-b6b9c8185839-Linux-x86_64.tgz"
export SPLUNK_HOME="/opt/splunkforwarder"  * if start command or other commands stop working reset the variable*
sudo mkdir $SPLUNK_HOME

sudo cp splunkforwarder-9.1.2-b6b9c8185839-Linux-x86_64.tgz /opt/splunkforwarder/ && cd /opt/splunkforwarder/

sudo tar -xzvC /opt/ -f splunkforwarder-9.1.2-b6b9c8185839-Linux-x86_64.tgz

sudo ./splunk enable boot-start -user splunkfwd --accept-license
sudo $SPLUNK_HOME/bin/splunk start --accept-license #create username and password
cd /opt/splunkforwarder/bin/
sudo ./splunk add forward-server YourSplunkserverIP:9997
sudo ./splunk add monitor /var/log
sudo./splunk restart

setfacl -m g:splunkfwd:r /var/log/secure             #any secure file if not showing in splunk
setfacl -m g:splunkfwd:r /var/log/messages

#change YourSplunkserverIP