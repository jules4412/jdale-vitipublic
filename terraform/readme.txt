Create a terraform project that will deploy a Wordpress website with a RDS Mysql database in the backend.

*This requires an existing AWS VPC and Subnets 

1) create s3 backend with lock using dynammodb in state tf and main.tf 

2) Then add resources to create SG for RDS Database and wordpress EC2 SG and resource to create RDS MYSQL Database  

3) Grab the Endpoint by retriving it from AWS RDS connections tab
or run the following Aws Cli command below after database has been setup

aws rds describe-db-instances   --query "*[].[DBInstanceIdentifier,Endpoint.Address,Endpoint.Port,MasterUsername]"
4) Then create Wordpress EC2 Server using userdata

For the  userdata script to work, use this block to call your script in terraform:



// -----------------------------------------------
// Change USERDATA varible value after grabbing RDS endpoint info 
// -----------------------------------------------
data "template_file" "user_data" {
  template = file("userdata1.sh")
  vars = {
    db_username      = var.database_user
    db_user_password = var.database_password
    db_name          = var.database_name
    db_RDS           = aws_db_instance.wordpressdb.endpoint
  }
}

Then Add Outputs Section

The main.txt is how your final main.tf will look. Have fun !